Client Installation
===================

Official Docs for the PolarMine XCH Client.

Getting Started
---------------

Check out our `Releases <https://github.com/PolarMine-Pooling/XCH-Client/releases>`_ page for the latest stable release.

This will be tagged with `Latest Release`.


Match your Operating System with the correct file type:

+--------------------+------------------+
|  Operating System  |  File Extension  |
+====================+==================+
|      Windows       |      .zip        |
+--------------------+------------------+
|   Ubuntu/Debian    |      .deb        |
+--------------------+------------------+
|      Mac OS        |      .dmg        |
+--------------------+------------------+


Ubuntu Installation
-------------------

Open your favorite terminal and run:

.. code-block:: bash

   curl https://github.com/PolarMine-Pooling/XCH-Client/releases/download/0.1.8.0/PolarMine-XCH-Build.deb
   sudo dpkg -i PolarMine-XCH-Client.deb
   sudo apt install libyaml-dev
   cd /usr/share/polarmine-xch
   python3 setup.py
   ./polarmine-xch --xch



Windows Installation
--------------------


1. Download the .zip file `here <https://github.com/PolarMine-Pooling/XCH-Client/releases/download/0.1.8.0/PolarMine-XCH-Build.zip>`_.

2. Unzip the .zip file.

3. Run the following commands in Powershell:

.. code-block:: bash

   cd ~/Downloads/PolarMine-XCH-Build/builds/package_windows/
   python setup.py
   .\polarmine-xch --xch


Note: if you cannot run Python3 with 'python', try 'python3' instead. If neither work, please install Python `here <https://www.python.org/>`_.


License
-------

Copyright 2021 PolarMine under the Apache License, Version 2.0.

.. toctree::
   :hidden:
   :caption: Table of Contents
