# Contributing to the PolarMine Docs

The process for contributing to this repository is as follows:

1. By submitting a pull request you are agreeing to our Contributor License
   Agreement. We are not asking you to assign copyright to us, but to give us
   the right to distribute  your changes without restriction. We ask this of all 
   contributors in order to assure our users of the origin and continuing
   existence of the code.

2. Rebase your changes.
   Update your local repository with the most recent code from the main
   repository, and rebase your branch on top of the latest master
   branch. We prefer your changes to be squashed into a single commit for easier
   backporting.

3. Submit a pull request. Push your local changes to your forked copy of the
   repository and submit a pull request. In the pull request, describe what your
   changes do and mention the number of the issue where discussion has taken
   place, eg “Closes #123″.

Then sit back and wait. There will probably be a discussion about the pull
request and, if any changes are needed, we would love to work with you to get
your pull request merged into our documentation.
