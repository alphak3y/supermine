PolarMine
==========

.. image:: https://gblobscdn.gitbook.com/assets%2F-Me7bQWDMQC-iYHYiLPi%2F-MeCoZsnzXpdeVbLvmbs%2F-MeCon5X5TBxuaotIF2q%2Fmedium%20banner%202%20(2).jpg?alt=media&token=58c2a05e-b821-4c0f-9c2a-5b8b4f3b2d5b


Official Documentation for PolarMine.

Getting Started
---------------

Take part in our Liquidity Generation Event by heading over to over to the :ref:`LGE` page.


Web3 Wallet Support
-------------------

* Metamask


Features
--------

PolarMine unique features include:

* Liquidity Sweeping
* Empire Router
* Empire Pair
* Empire Factory


License
-------

Copyright 2021 PolarMine under the Apache License, Version 2.0.
